﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using TMPro;
using UnityEngine;

public class SocketToServer : MonoBehaviour
{
    public string Host = "127.0.0.1";
    public ushort Port = 1111; //0-65,535
    public TextMeshProUGUI ChatDisplay;
    public TMP_InputField InputDisplay;


    private TcpClient socket = new TcpClient();

    // Start is called before the first frame update
    void Start()
    {
        InputDisplay.interactable = false;
        InputDisplay.onEndEdit.AddListener(OnInputFieldEdit);
        ConnectToServer();
    }

    private async void ConnectToServer()
    {
        try
        {
            await socket.ConnectAsync(Host, Port);
            AddMessageToChatDisplay("<color=#005500>Successfully connected to server</color>");
            InputDisplay.interactable = true;
        }
        catch (Exception e)
        {
            AddMessageToChatDisplay("<color=#550000>Error: " + e.Message + "</color>");
            return;
        }

        while (true)
        {
            byte[] data = new byte[socket.Available];
            await socket.GetStream().ReadAsync(data, 0, data.Length);
            if (data.Length > 0)
            {
                var input = Encoding.ASCII.GetString(data);
                var breakdown = input.Split('\t');
                if(breakdown[0] == "CHAT")
                { 
                    input = input.Substring(5, input.Length - 6);
                }
                AddMessageToChatDisplay(input);
            }
                
        }

    }

    public void AddMessageToChatDisplay(string text)
    {
        ChatDisplay.text += text + "\n";
    }

    public void OnInputFieldEdit(string s)
    {
        SendMessageToServer(s);
        InputDisplay.text = "";
        InputDisplay.Select();
        InputDisplay.ActivateInputField();
    }

    public void SendMessageToServer(string message)
    {
        //TODO: create packets
        if (message[0] == '!')
        {
            message = message.Substring(1, message.Length - 1);
            var breakdown = message.Split(' ');

            switch (breakdown[0].ToLower())
            {
                case "name":
                    message = message.Substring(5, message.Length - 5);
                    message = "NAME\t" + message + "\n";
                    break;
                case "dm":
                case "msg":
                case "whisper":
                    message.Substring(breakdown[1].Length + 1, message.Length - breakdown[1].Length - 1);
                    message = "DMSG\t" + breakdown[1] + "\t" + message + "\n";
                    break;
                case "list":
                    message = "LIST\n";
                    break;
            }

        }
        else
        {
            message = "CHAT\t" + message + "\n";
        }
        byte[] data = Encoding.ASCII.GetBytes(message);
        socket.GetStream().Write(data, 0, data.Length);
    }

}
